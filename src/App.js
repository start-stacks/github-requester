import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

class App extends Component {
  constructor () {
    super()

    this.state = {
      username: '',
      blog: ''
    }

    this.handleClick = this.handleClick.bind(this)
  }

  handleClick () {
    axios.get('https://api.github.com/users/charliesolomon')
      .then(response => this.setState({
        username: response.data.name,
        blog: response.data.blog
      }))

    console.log(name)
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <button className='button' onClick={this.handleClick}>Click Me</button>
        <p>Full Name: {this.state.username}</p>
        <p>My Blog: {this.state.blog}</p>
      </div>
    );
  }
}

export default App;
